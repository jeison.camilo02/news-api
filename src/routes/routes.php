<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt' ])->group(function () {
Route::group([
    'prefix' => 'api/v1'
], function () {
    Route::apiResource('news', 'Inmovsoftware\NewsApi\Http\Controllers\V1\NewsController');
    Route::apiResource('news_category', 'Inmovsoftware\NewsApi\Http\Controllers\V1\NewsCategoryController');
        });
});
