<?php

namespace Inmovsoftware\NewsApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $table = "it_news";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'photo', 'it_categories_news_id', 'creation_date', 'update_date', 'publication_date', 'close_date', 'text', 'status'];


    public function NewsCategory()
    {
        return $this->belongsTo('Inmovsoftware\NewsApi\Models\V1\NewsCategory', 'it_categories_news_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->it_users = auth()->user()->id;
    }



}
