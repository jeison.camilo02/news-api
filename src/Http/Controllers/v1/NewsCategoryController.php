<?php
namespace Inmovsoftware\NewsApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\NewsApi\Models\V1\News;
use Inmovsoftware\NewsApi\Models\V1\NewsCategory;
use Inmovsoftware\NewsApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NewsCategoryController extends Controller
{
    public function index(Request $request)
    {
        $filter = "name";
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "name";

        $item = News::orderBy($sortField, $sortOrder);
        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        $item->where('status', 'A');
        return new GlobalCollection($item->paginate($pageSize));
    }

    public function index_select(Request $request)
    {
        $item = Position::orderBy("name", "asc");
        $item->where('status', 'A');
        return response()->json($item->get());
    }



    public function store(Request $request)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);
        $InsertId = Position::insertGetId($data);
        $inserted = Position::where("id", $InsertId)->get();
        return response()->json($inserted);
    }


    /**
     * Display the specified resource.
     *
     * @param  Inmovsoftware\PositionApi\Models\V1\Position $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {

        return response()->json($position);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Inmovsoftware\PositionApi\Models\V1\Position $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);

        $company->update($data);
        return response()->json($company);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inmovsoftware\PositionApi\Models\V1\Position $position
     * @return \Illuminate\Http\Response
     */
    public function destroy(Position $position)
    {
        $item = $position->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }

}
